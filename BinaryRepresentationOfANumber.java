package assignment3;
import java.util.*;
public class BinaryRepresentationOfANumber {

	public static void main(String[] args) {
		int decimal_number, reminder, quotient, i=1, j;  
        int binary_number[] = new int[100];  
        Scanner scan = new Scanner(System.in);  
          
        System.out.print("Input a Decimal Number : ");  
        decimal_number = scan.nextInt();  
          
        quotient = decimal_number;  
          
        while(quotient != 0)  
        {  
            binary_number[i++] = quotient%2;  
            quotient = quotient/2;  
        }  
		String binary_str="";
		System.out.print("Binary number is: ");  
        for(j=i-1; j>0; j--)  
        {  
          binary_str = binary_str + binary_number[j];	
        }  
		System.out.print(binary_str);
        i = binary_str.length()-1;
        while(binary_str.charAt(i)=='0') {
            i--;
        }
        int length = 0;
        int ctr = 0;
        for(; i>=0; i--) {
            if(binary_str.charAt(i)=='1') {
                length = Math.max(length, ctr);
                ctr = 0;
            } else {
                ctr++;
            }
        }
        length = Math.max(length, ctr);
        System.out.println("\nLength of the longest sequence: "+length);
    }
	}
